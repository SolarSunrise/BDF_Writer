#include <QCoreApplication>
#include "serialmonitor.h"
#include "guiconsole.h"
#include <signal.h>

static void CleanUp(int sig){
    qApp->quit();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    signal(SIGINT, CleanUp);

    SerialMonitor test;


    return a.exec();
}
