#-------------------------------------------------
#
# Project created by QtCreator 2014-11-13T13:23:43
#
#-------------------------------------------------

QT       += core serialport multimedia

QT       -= gui

# FIX THIS SHIT
# For Windows
LIBS        += -lfftw3-3
LIBS     += -lncursesw

# For Linux
# LIBS        += -lfftw
# LIBS     += -lncurses

TARGET = BDF_Writer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CFLAGS_RELEASE -= -O
QMAKE_CFLAGS_RELEASE -= -O1
QMAKE_CFLAGS_RELEASE -= -O2

# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE *= -O3
QMAKE_CFLAGS_RELEASE *= -O3

SOURCES += main.cpp\
        serialmonitor.cpp \
        edflib.c \
        guiconsole.cpp \
        filterIIR.cpp \
        remDetect.cpp \
        IIR_Coeffs.cpp

HEADERS  += serialmonitor.h \
        edflib.h \
        guiconsole.h \
        filterIIR.h \
        remDetect.h
