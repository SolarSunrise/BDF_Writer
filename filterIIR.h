class filterIIR
{
public:
    filterIIR(double *iirCoeffs, int stages);

    double SectCalcForm2(int k, double x);
    void RunIIRBiquadForm2(double *Input, double *Output, int NumSigPts);
    void filtfilt(double *data, double *data_out, int filter_size);
    void reverse(double arr[], int count);

private:
    double *m_iirCoeffs;
    int m_stages;

    double buffer0[100], buffer1[100], buffer2[100];

 };
