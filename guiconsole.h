#ifndef GUICONSOLE_H
#define GUICONSOLE_H
#include <ncursesw/ncurses.h>
// #include <ncurses.h>

class guiConsole
{
public:
    guiConsole();
    ~guiConsole();

    void update_display();
    void touch_all();

    void update_Impedance(int imp_values[], int channels);
    void update_Time(QString current_time);
    void update_Config(QString string_Config);
    void update_Toolbar(QString string_Status);

    WINDOW *display_Impedance;
    WINDOW *display_Duration;
    WINDOW *display_Configs;
};

#endif // GUICONSOLE_H
