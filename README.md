## Software that recieves data from BioEXG
 - Saves **24 bit** data to BDF file
 - Can configure # of channels + individual *channel labels*
 - Allows user to **configure ADS1299** by accessing SETTING_MODE in BioEXG through SPP
 - **Real time Impedence Monitor** - Does not work with active electrodes! Only passive electrodes. 
 - **REM detection with various stimulus options** - external GPIO trigger and .wav playback
 - 
 - ~~TODO: Option to save configuration and load configuration for BioEXG and BDF_Writer~~

