set multiplot layout 4, 1 title "REM Analysis on Frequency band 8hz to 16hz"

unset key

set style line 1 lt 1 lw 2 lc rgb "red"
set style line 2 lt 1 lw 2 lc rgb "purple"
set style line 3 lt 1 lw 2 lc rgb "blue"
set style line 4 lt 7 lw 2 lc rgb "cyan"

set title "SEFd - 8 to 16hz"
set ylabel "Hz"
set xdata time
set timefmt "%s"
set yrange[4:5]
plot 'analysis_2015-08-26_T01-41.txt' using 1:2 with lines ls 1

set title "Relative Power - 8 to 16hz"
set ylabel "dB"
set xdata time
set timefmt "%s"
set yrange[-15:-9]
plot 'analysis_2015-08-26_T01-41.txt' using 1:3 with lines ls 2

set title "Absolute Power - 8 to 16hz"
set ylabel "dB"
set xdata time
set timefmt "%s"
set yrange[13:20]
plot 'analysis_2015-08-26_T01-41.txt' using 1:4 with lines ls 3

set title "REM Detection Algorithm (Decision tree) Output"
set ylabel "REM"
set xdata time
set timefmt "%s"
set yrange[-0.1:1.1]
plot 'analysis_2015-08-26_T01-41.txt' using 1:5 with linespoints ls 4
